
#Booleanos
p = True
q = False
r: bool = True
#and es verdad cuando todas las variables son verdad
if p and q:
    mensaje = "Es verdad"
    print(mensaje)
else:
    print("Es falso")

#or es falso cuando todas las variables sean falsas
if p or q:
    print("Verdad en OR")
else:
    print('Falso en OR')

if not q:
    print("Cumple la condición")
else:
    print("No cumple la condición")

if not q and p:
    print("Cumple condición 2")
else:
    print("No cumple condición 2")

num_1 = 100
num_2 = 50
num_3 = 200
num_4 = 80

if num_1 > num_2:
    print("num_1 > num_2")
else:
    print("num_1 NO es mayor a num_2")

if not(num_1 > num_2 and num_4 < num_2 ):
    print("Numple condición")
else:
    print("no cumple condición")

if 's' == 'S':
    print("son iguales")
else:
    print("no son iguales")

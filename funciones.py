
def saludar():
    mensaje = "Hola mundo"
    print(mensaje)

saludar()

def saludar_con_retorno():
    mensaje = "Hola mundo desde una función con return"
    return mensaje

respuesta = saludar_con_retorno()
#respuesta = "Respuesta = "+respuesta
respuesta += " Esto es otra cadena"
print(respuesta)

def sumar():
    return 5 + 10

#num = numero()

'''
def nombre_funcion( defino si tendrá parámetros ):
    mi código
'''

#Funciones con parámetros
def sumar_con_parametros(num_1, num_2):
    return num_1+num_2

resp_1 = sumar_con_parametros(10, 5.5)
#print(f"resp_1 = {resp_1}")
print('resp_1 = %.2f' %resp_1)
resp_2 = sumar_con_parametros(resp_1, 20)
#Mostrando mas d euna variable

print("resp_1 = %.2f y resp_2 = %.2f" %(resp_1, resp_2))

#redondear a un número determinado de flotantes (decimales)
#round(numero_a_redondear, numero_decimales)
redondear = round(resp_1, 2)
print(f"redondear = {redondear}")

'''
def indicar_mayor_edad(nombre: str, edad: int):
    if(edad >= 18):
        return f"{nombre} es mayor de edad"
    else:
        return f"{nombre} es menor de edad"
'''

def indicar_mayor_edad(nombre: str, edad: int):
    mensaje = ''
    if(edad >= 18):
        mensaje = f"{nombre} es mayor de edad"
    else:
        mensaje = f"{nombre} es menor de edad"
    return mensaje

print( indicar_mayor_edad('Kevin', 20) )


#Entero
num_1 = 10
#indicar el tipo de dato
'''
Indicar el tipo de dato es ÚNICAMENTE por legibilidad de código
'''
num_2: int = 5

#print -> muestra mensaje en consola
#print( type(num_2) )

#flotantes
num_2 = 10.1
#type -> retorna el tipo de dato que almacena una variable
#print(type(num_2))

#String
texto = "Esto es un string"
mensaje: str = "Esto es un mensaje para el usuario"

#----STRING------
concatenar = texto +' '+ mensaje
print(concatenar)

#Formatear salida
concatenar = f'{texto} {mensaje}'
print(concatenar)


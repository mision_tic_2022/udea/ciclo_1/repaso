'''
1) Desarrolle un programa que pida cinco notas de un estudiante, al final debe
    mostrar el promedio (usando while)
2) Desarrolle una función que imprima la sumatoria de los números impares comprendidos
    entre 50 y 200. (utilizando while)
    NOTA:
        % -> Retorna el residuo de una división.
        si 4%2 -> 0 entonces 4 es par sino 4 es impar
'''

# Solución de Santiago

def promedio_notas():
    contador = 1
    nota = 0
    while contador <= 5:
        notas = int(input('Ingrese ingrese la nota: '))
        nota += notas
        contador += 1
    print('El promedio de sus notas es:', nota/5)


# Solución de ANdrés
acum_nota = 0
i = 0
print("-----------------------------------------------------")
while i < 5:
    try:
        nota = float(input(f"Ingrese la nota #{i} del estudiante: "))
        acum_nota += nota
        i += 1
    except:
        print("Debe ingresar un número!")
print(i)

print(f"\nla nota final del estudiante es: {acum_nota/5}")


# Juan Felipe
def menu():
    texto = "Ingrese nota >>> "
    acumulador = 0
    contador = 0
    dato = ''
    # Iteracción
    while contador < 5:
        try:
            # Solicitando un dato al usuario
            dato = input(texto)
            acumulador += int(dato)
            print(acumulador)
            contador += 1
        except:
            print('Debe ingresar un número')
    acumulador /= 5
    print('El ciclo ha terminado con acumulado en ', acumulador)


#menu()

# Solución de gilberto
promedio: int = 0
termino: bool = False
contador = 1
while not termino:
    try:
        nota: float = float(input(F'ingrese nota # {contador} >>>'))
        promedio += nota
        contador += 1
        if contador > 5:
            termino = True
            promedio /= 5
    except:
        print('Debe ingresar un numero')
print(F'Su nota promedio es de {promedio}')





#----------Punto 2---------
#Solucinado por Andrés Felipe
def sumatoria():
    x = 50
    suma = 0
    while x <= 200:
        if x%2 != 0:
            print(x)
            suma += x
        x += 1
    print(f'la sumatoria es: {suma}')
sumatoria()

#Gustavo Andrés
def sumatoriaprimos():
    inicio1= int(input('ingrese inicio de rango'))
    fin1= int(input('ingrese fin del rango'))
    bandera = 0
    total = 0
    while inicio1 < fin1:
        bandera = inicio1%2
        if bandera != 0:
            total += inicio1
        inicio1 += 1
        print(inicio1)
    print('total = %u' %total)

sumatoriaprimos()

'''
Desarrollar una función que retorne la sumatoria de los números
de 1 a 1000
'''

def sumar_numeros(inicio: int, fin: int):
    suma = 0
    while inicio <= fin:
        suma += inicio
        inicio += 1
    return suma

print(sumar_numeros(1, 1000000))
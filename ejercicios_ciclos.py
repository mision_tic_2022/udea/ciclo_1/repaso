
#Utilizando break
'''
def menu():
    texto = "Ingrege 's' para salir ó ingrese un número para acumular >>> "
    acumulador = 0
    #Iteración
    while acumulador < 100:
        try:
            #Solicitando un dato al usuario
            dato = input(texto)
            #Verificamos si el usuario quiere salir
            if dato == 's':
                break
            else:
                #Acumulamos el número ingresado
                acumulador += int(dato)
                print(acumulador)
        except:
            print('Debe ingresar un número')
    
    print('El ciclo ha terminado con acumulado en ', acumulador)

menu()
'''

#break -> rompe un ciclo
'''
while True:
    num = int( input('Ingrese número >>> ') )
    if num == 1:
        #rompe el ciclo
        break
'''

'''
Uso de cadena de caracteres, (upper, lower), rebanando
'''


def menu():
    texto = "Ingrege 's' para salir ó ingrese un número para acumular >>> "
    acumulador = 0
    dato = ''
    #Iteración
    while acumulador < 100 and dato != 's':
        try:
            #Solicitando un dato al usuario
            dato =  input(texto)
            if dato != 's':
                acumulador += int(dato)
                print(acumulador)
        except:
            print('Debe ingresar un número')
    
    print('El ciclo ha terminado con acumulado en ', acumulador)

menu()